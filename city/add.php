<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2> form</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="city">City:</label>

            <select class="alert-danger">
                <option value="ctg">ctg</option>
                <option value="dhk">dhk</option>
                <option value="syl">syl</option>
                <option value="com">com</option>
            </select>
        </div>

        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2>Vertical (basic) form</h2>
    <form>
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="title" class="form-control" id="title" placeholder="Enter title">
        </div>
        <div class="form-group">
            <label for="athr">Author:</label>
            <input type="author" class="form-control" id="author" placeholder="Enter password">
        </div>

        <button type="submit" class="btn btn-primary">Add</button>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

</body>
</html>
